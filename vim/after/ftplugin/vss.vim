" Local settings
set nowrap
set showtabline=0
function! vss#Statusline(pat)
    let sum = 0.00
    for i in getline(1,line('$'))
        if i->matchstr(a:pat)->len() > 0
            let sum += i->split()[6]->str2float()
        endif
    endfor
    return printf('%.2f', sum)
endfunction
set statusline=%#Question#%=OPEN:\ %{vss#Statusline('OPEN$')}\ \ CLOSED:\ %{vss#Statusline('CLOSED[#0-9\ -]*$')}
set nolist
set cursorline
set matchpairs=

" Local mappings
nnoremap <buffer> <silent> <expr> <C-s> getline('.')->matchstr('OPEN$\|CLOSED[#0-9 -]*$')->len() > 0
                    \ ? ":echoerr strftime('%Y-%m-%d %H-%M')<CR>"
                    \ : getline('.')->matchstr('OPEN$')->len() > 0
                        \ ? ":call getline('.')->substitute('OPEN$', 'CLOSED    #' . strftime('%Y-%m-%d'), '')->setline('.')<CR>"
                        \ : ":call getline('.')->substitute('CLOSED[#0-9 -]*$', 'OPEN', '')->setline('.')<CR>"
