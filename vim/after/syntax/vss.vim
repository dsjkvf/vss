" Syntax
syntax match vssTitle  /DATE          TICKER  PRICE     AMOUNT     QNT     CLOSE      BALANCE  STATUS/
syntax match vssClosed /^.* CLOSED[#0-9 -]*$/

" Highlights
hi! def link vssClosed Comment
hi! def link vssTitle  Title

" Options
hi CursorLine guibg=darkblue
